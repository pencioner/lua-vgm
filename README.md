This is library for manipulation of VGM files written in Lua language. It is in early alpha stage for now, and not all the VGM features are implemented so far.

To run examples you need only to install Lua version 5.3 as there are no other
external dependencies.

To run some examples, clone repo and run:
```
LUA_PATH="$LUA_PATH;/path/to/lua-vgm/lib/?.lua" lua5.3 ./examples/vgm_print.lua your_music_file.vgz 
```


Support for LuaRocks and some Windows installation planned to be done sooner.


