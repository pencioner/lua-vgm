local vgmfile = require 'vgmfile'
local vgmheader = require 'vgmheader'
local vgmchips = require 'vgmchips'
local vgmoutput = require 'vgmoutput'

local VCH = vgmchips.VGM_CHIPS
local parseVgm = vgmfile.parseVgm
local produceVgm = vgmoutput.produceVgm

local filename = ({...})[1]
local vgm, content = parseVgm(filename)

-- now just remove it because there's no support of GD3 yet
vgm.header.GD3_offset = 0

local new_data = {}

for _, cmd_data in pairs(vgm.data) do
  if cmd_data.chip == VCH.SN764x then
    local chan_bits = cmd_data.value & 0xF0
    -- detect noise channel tone and volume cmds and skip 'em
    if chan_bits == 0xF0 or chan_bits == 0xE0 then
      cmd_data = nil
    end
  end
  new_data[1+#new_data] = cmd_data
end

vgm.data = new_data
local ofile = io.open(filename.."_filtered.vgm", "wb")
ofile:write(produceVgm(vgm))
ofile:close()

