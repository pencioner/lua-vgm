local chips = require 'vgmchips'
local VGM_CHIPS = chips.VGM_CHIPS

local utils = require 'utils'
local consumeBytes = utils.consumeBytes

local SPECIAL_DATA_BANK = {"DATA_BANK"}  -- unique table for special case
local SPECIAL_REGISTER_GAMEGEAR_STEREO = {"GAME_GEAR_PSG_STEREO"}  -- // --
local SPECIAL_REGISTER_PSG = {"PSG"}  -- // --

local VGM_OP_UNKNOWN = -1
local VGM_OP_OTHER = 0
local VGM_OP_REG_WRITE = 1
local VGM_OP_MEM_WRITE = 2
local VGM_OP_SET_BANK_OFFSET = 3
local VGM_OP_DATA_BLOCK = 4
local VGM_OP_EODB = 5
local VGM_OP_PCM_RAM_WRITE = 6
local VGM_OP_SEEK_OFFSET = 7

-- first value in table is the length of param data (num bytes to consume)
local VGM_COMMANDS = {
  [0x4F] = { 1, chip = VGM_CHIPS.SN764x },  -- GameGear stereo panning
  [0x50] = { 1, chip = VGM_CHIPS.SN764x },
  [0x30] = { _second_chip_of = 0x50 },
  [0x3F] = { _second_chip_of = 0x50 },  -- GameGear stereo
  [0x51] = { 2, chip = VGM_CHIPS.YM2413 },
  [0xA1] = { _second_chip_of = 0x51 },
  [0x52] = { 2, chip = VGM_CHIPS.YM2612 },
  [0xA2] = { _second_chip_of = 0x52 },
  [0x53] = { _second_port_of = 0x52 },
  [0xA3] = { _second_chip_of = 0x53 },
  [0x54] = { 2, chip = VGM_CHIPS.YM2151 },
  [0xA4] = { _second_chip_of = 0x54 },
  [0x55] = { 2, chip = VGM_CHIPS.YM2203 },
  [0xA5] = { _second_chip_of = 0x55 },
  [0x56] = { 2, chip = VGM_CHIPS.YM2608 },
  [0xA6] = { _second_chip_of = 0x56 },
  [0x57] = { _second_port_of = 0x56 },
  [0xA7] = { _second_chip_of = 0x57 },
  [0x58] = { 2, chip = VGM_CHIPS.YM2610 },
  [0xA8] = { _second_chip_of = 0x58 },
  [0x59] = { _second_port_of = 0x58 },
  [0xA9] = { _second_chip_of = 0x59 },
  [0x5A] = { 2, chip = VGM_CHIPS.YM3812 },
  [0xAA] = { _second_chip_of = 0x5A },
  [0x5B] = { 2, chip = VGM_CHIPS.YM3526 },
  [0xAB] = { _second_chip_of = 0x5B },
  [0x5C] = { 2, chip = VGM_CHIPS.Y8950 },
  [0xAC] = { _second_chip_of = 0x5C },
  [0x5D] = { 2, chip = VGM_CHIPS.YMZ280B },
  [0xAD] = { _second_chip_of = 0x5D },
  [0x5E] = { 2, chip = VGM_CHIPS.YMF262 },
  [0xAE] = { _second_chip_of = 0x5E },
  [0x5F] = { _second_port_of = 0x5E },
  [0xAF] = { _second_chip_of = 0x5F },
  [0x61] = { 2 },
  [0x62] = { 0 },
  [0x63] = { 0 },
  [0x64] = { 3, not_implemented = true },
  [0x66] = { special_command = true },
  [0x67] = { special_command = true },
  [0x68] = { special_command = true },
  [0xA0] = { 2, chip = VGM_CHIPS.AY8910 },
  [0xB0] = { 2, chip = VGM_CHIPS.RF5C68 },
  [0xB1] = { 2, chip = VGM_CHIPS.RF5C164 },
  [0xB2] = { 2, chip = VGM_CHIPS.PWM },
  [0xB3] = { 2, chip = VGM_CHIPS.GameBoy_DMG },
  [0xB4] = { 2, chip = VGM_CHIPS.NES_APU },
  [0xB5] = { 2, chip = VGM_CHIPS.MultiPCM },
  [0xB6] = { 2, chip = VGM_CHIPS.uPD7759 },
  [0xB7] = { 2, chip = VGM_CHIPS.OKIM6258 },
  [0xB8] = { 2, chip = VGM_CHIPS.OKIM6295 },
  [0xB9] = { 2, chip = VGM_CHIPS.HuC6280 },
  [0xBA] = { 2, chip = VGM_CHIPS.K053260 },
  [0xBB] = { 2, chip = VGM_CHIPS.Pokey },
  [0xBC] = { 2, chip = VGM_CHIPS.WonderSwan },
  [0xBD] = { 2, chip = VGM_CHIPS.SAA1099 },
  [0xBE] = { 2, chip = VGM_CHIPS.ES5506 },
  [0xBF] = { 2, chip = VGM_CHIPS.GA20 },
  [0xC0] = { 3, chip = VGM_CHIPS.Sega_PCM },
  [0xC1] = { 3, chip = VGM_CHIPS.RF5C68 },
  [0xC2] = { 3, chip = VGM_CHIPS.RF5C164 },
  [0xC3] = { 3, chip = VGM_CHIPS.MultiPCM },
  [0xC4] = { 3, chip = VGM_CHIPS.QSound },
  [0xC5] = { 3, chip = VGM_CHIPS.SCSP },
  [0xC6] = { 3, chip = VGM_CHIPS.WonderSwan },
  [0xC7] = { 3, chip = VGM_CHIPS.VSU },
  [0xC8] = { 3, chip = VGM_CHIPS.X1_010 },
  [0xD0] = { 3, chip = VGM_CHIPS.YMF278B },
  [0xD1] = { 3, chip = VGM_CHIPS.YMF271 },
  [0xD2] = { 3, chip = VGM_CHIPS.SCC1 },
  [0xD3] = { 3, chip = VGM_CHIPS.K054539 },
  [0xD4] = { 3, chip = VGM_CHIPS.C140 },
  [0xD5] = { 3, chip = VGM_CHIPS.ES5503 },
  [0xD6] = { 3, chip = VGM_CHIPS.ES5506 },
  [0xE0] = { 4, chip = VGM_CHIPS.YM2612 },
  [0xE1] = { 4, chip = VGM_CHIPS.C352 },
}

-- resolve refs for chips which use different cmd for second port of chip
for cmd, cmd_data in pairs(VGM_COMMANDS) do
  local second_port_of = cmd_data._second_port_of
  if second_port_of then
    local first_port = VGM_COMMANDS[second_port_of]
    VGM_COMMANDS[cmd] = utils.table_deepcopy(first_port)
    VGM_COMMANDS[cmd].port = 1
    first_port.port = 0
  end
end

-- resolve refs for chips which use different cmd for 2nd chip instance
for cmd, cmd_data in pairs(VGM_COMMANDS) do
  local second_chip = cmd_data._second_chip_of
  if second_chip then
    VGM_COMMANDS[cmd] = utils.table_deepcopy(VGM_COMMANDS[second_chip])
    VGM_COMMANDS[cmd].chip_no = 2
  end
end

-- those parser function below consume VGM data and fill in command data
local raw_data_parser_dd = function(data)
  data.value = data.bytes[1]
end

-- 0xB2 rD dd : PWM, write value ddd to register r (D is MSB, dd is LSB)
local raw_data_parser_rD_dd = function(data)
  local bytes = data.bytes
  data.value = (bytes[1] & 0x0F) * 256 + bytes[2]
  data.register = bytes[1] >> 4
end

local raw_data_parser_rr_dd = function(data)
  local bytes = data.bytes
  data.register = bytes[1]
  data.value = bytes[2]
end

-- honor the higher bit of first param data as a 2nd chip command
local raw_data_parser_DUAL_rr_dd = function(data)
  local bytes = data.bytes
  local register = bytes[1]
  if register & 0x80 ~= 0 then
    data.chip_no = 2
  end
  data.register = register & 0x7F
  data.value = bytes[2]
end

local raw_data_parser_mmMM_dd = function(data)
    local bytes = data.bytes
    data.address = bytes[2] * 256 + bytes[1]
    data.value = bytes[3]
    data.op = VGM_OP_MEM_WRITE
end

local raw_data_parser_cc_mmMM = function(data)
  local bytes = data.bytes
  data.register = bytes[1]  -- channel number
  data.address = bytes[3] * 256 + bytes[2]
  data.op = VGM_OP_SET_BANK_OFFSET
end

local raw_data_parser_DDdd_rr = function(data)
  local bytes = data.bytes
  data.value = bytes[1] * 256 + bytes[2]
  data.register = bytes[3]
end

local raw_data_parser_MMmm_dd = function(data)
  local bytes = data.bytes
  data.address = bytes[1] * 256 + bytes[2]
  data.value = bytes[3]
  data.op = VGM_OP_MEM_WRITE
end

local raw_data_parser_pp_rr_dd = function(data)
  local bytes = data.bytes
  data.port = bytes[1]
  data.register = bytes[2]
  data.value = bytes[3]
end

local raw_data_parser_RRrr_dd = function(data)
  local bytes = data.bytes
  data.register = bytes[1] * 256 + bytes[2]
  data.value = bytes[3]
end

local raw_data_parser_rr_DDdd = function(data)
  local bytes = data.bytes
  data.register = bytes[1]
  data.value = bytes[2] * 256 + bytes[3]
end

local raw_data_parser_RRrr_DDdd = function(data)
  local bytes = data.bytes
  data.register = bytes[1] * 256 + bytes[2]
  data.value = bytes[3] * 256 + bytes[4]
end

local raw_data_parser_wait = function(data)
  data.wait = data.cmd - 0x6F
end

local raw_data_parser_wait_wwWW = function(data) 
  local bytes = data.bytes
  data.wait = bytes[2] * 256 + bytes[1]
end

-- 0x8n: YM2612 port 0 address 2A write from the data bank,
-- then wait n samples; n can range from 0 to 15
local raw_data_parser_YM2612_DATA_BANK = function(data)
  data.wait = data.cmd - 0x80
  data.port = 0
  data.chip = VGM_CHIPS.YM2612
  data.register = 0x2A
  data.value = SPECIAL_DATA_BANK
end

-- 0xE0: Seek to offset dddddddd (Intel byte order) in PCM data bank
-- of data block type 0 (YM2612).
local raw_data_parser_a4a3a2a1 = function(data)
  local bytes, address = data.bytes, 0
  for idx = 4, 1, -1 do
    address = address * 256 + bytes[idx]
  end
  data.address = address
  data.chip = VGM_CHIPS.YM2612
  data.op = VGM_OP_SEEK_OFFSET
end


-- commands with no following data
local chip_data_parsers_0 = {
  [0x62] = function(data) data.wait = 735 end,
  [0x63] = function(data) data.wait = 882 end,
  [0x70] = raw_data_parser_wait,
  [0x71] = raw_data_parser_wait,
  [0x72] = raw_data_parser_wait,
  [0x73] = raw_data_parser_wait,
  [0x74] = raw_data_parser_wait,
  [0x75] = raw_data_parser_wait,
  [0x76] = raw_data_parser_wait,
  [0x77] = raw_data_parser_wait,
  [0x78] = raw_data_parser_wait,
  [0x79] = raw_data_parser_wait,
  [0x7A] = raw_data_parser_wait,
  [0x7B] = raw_data_parser_wait,
  [0x7C] = raw_data_parser_wait,
  [0x7D] = raw_data_parser_wait,
  [0x7E] = raw_data_parser_wait,
  [0x7F] = raw_data_parser_wait,
  [0x80] = raw_data_parser_YM2612_DATA_BANK,
  [0x81] = raw_data_parser_YM2612_DATA_BANK,
  [0x82] = raw_data_parser_YM2612_DATA_BANK,
  [0x83] = raw_data_parser_YM2612_DATA_BANK,
  [0x84] = raw_data_parser_YM2612_DATA_BANK,
  [0x85] = raw_data_parser_YM2612_DATA_BANK,
  [0x86] = raw_data_parser_YM2612_DATA_BANK,
  [0x87] = raw_data_parser_YM2612_DATA_BANK,
  [0x88] = raw_data_parser_YM2612_DATA_BANK,
  [0x89] = raw_data_parser_YM2612_DATA_BANK,
  [0x8A] = raw_data_parser_YM2612_DATA_BANK,
  [0x8B] = raw_data_parser_YM2612_DATA_BANK,
  [0x8C] = raw_data_parser_YM2612_DATA_BANK,
  [0x8D] = raw_data_parser_YM2612_DATA_BANK,
  [0x8E] = raw_data_parser_YM2612_DATA_BANK,
  [0x8F] = raw_data_parser_YM2612_DATA_BANK,
  default = function(data) data.op = VGM_OP_UNKNOWN end,
}

-- commands with following 1 byte of data
local chip_data_parsers_1 = {
  [0x4F] = function(data)
    data.register = SPECIAL_REGISTER_GAMEGEAR_STEREO
    return raw_data_parser_dd(data)
  end,

  default = function(data)
    data.register = SPECIAL_REGISTER_PSG
    return raw_data_parser_dd(data)
  end,
}

-- commands with following 2 bytes of data
local chip_data_parsers_2 = {
  [0x61] = raw_data_parser_wait_wwWW,
  [0x51] = raw_data_parser_rr_dd,
  [0x52] = raw_data_parser_rr_dd,
  [0x53] = raw_data_parser_rr_dd,
  [0x54] = raw_data_parser_rr_dd,
  [0x55] = raw_data_parser_rr_dd,
  [0x56] = raw_data_parser_rr_dd,
  [0x57] = raw_data_parser_rr_dd,
  [0x58] = raw_data_parser_rr_dd,
  [0x59] = raw_data_parser_rr_dd,
  [0x5A] = raw_data_parser_rr_dd,
  [0x5B] = raw_data_parser_rr_dd,
  [0x5C] = raw_data_parser_rr_dd,
  [0x5D] = raw_data_parser_rr_dd,
  [0x5E] = raw_data_parser_rr_dd,
  [0x5F] = raw_data_parser_rr_dd,
  [0xA1] = raw_data_parser_rr_dd,
  [0xA2] = raw_data_parser_rr_dd,
  [0xA3] = raw_data_parser_rr_dd,
  [0xA4] = raw_data_parser_rr_dd,
  [0xA5] = raw_data_parser_rr_dd,
  [0xA6] = raw_data_parser_rr_dd,
  [0xA7] = raw_data_parser_rr_dd,
  [0xA8] = raw_data_parser_rr_dd,
  [0xA9] = raw_data_parser_rr_dd,
  [0xAA] = raw_data_parser_rr_dd,
  [0xAB] = raw_data_parser_rr_dd,
  [0xAC] = raw_data_parser_rr_dd,
  [0xAD] = raw_data_parser_rr_dd,
  [0xAE] = raw_data_parser_rr_dd,
  [0xAF] = raw_data_parser_rr_dd,
  [0xB2] = raw_data_parser_rD_dd,
  default = raw_data_parser_DUAL_rr_dd,
}

-- commands with following 3 bytes of data
local chip_data_parsers_3 = {
  [0xC0] = raw_data_parser_mmMM_dd,
  [0xC1] = raw_data_parser_mmMM_dd,
  [0xC2] = raw_data_parser_mmMM_dd,
  [0xC3] = raw_data_parser_cc_mmMM,
  [0xC4] = raw_data_parser_DDdd_rr,
  [0xC5] = raw_data_parser_MMmm_dd,
  [0xC6] = raw_data_parser_MMmm_dd,
  [0xC7] = raw_data_parser_MMmm_dd,
  [0xC8] = raw_data_parser_MMmm_dd,
  [0xD0] = raw_data_parser_pp_rr_dd,
  [0xD1] = raw_data_parser_pp_rr_dd,
  [0xD2] = raw_data_parser_pp_rr_dd,
  [0xD3] = raw_data_parser_RRrr_dd,
  [0xD4] = raw_data_parser_RRrr_dd,
  [0xD5] = raw_data_parser_RRrr_dd,
  [0xD6] = raw_data_parser_rr_DDdd,
  default = function(data) data.op = VGM_OP_UNKNOWN end,
}

-- commands with following 4 bytes of data
local chip_data_parsers_4 = {
  [0xE0] = raw_data_parser_a4a3a2a1,
  [0xE1] = raw_data_parser_RRrr_DDdd,
  default = function(data) data.op = VGM_OP_UNKNOWN end,
}

-- mapped by the number of consumed bytes after the command
local chip_data_parsers = {
  [0] = chip_data_parsers_0,
  [1] = chip_data_parsers_1,
  [2] = chip_data_parsers_2,
  [3] = chip_data_parsers_3,
  [4] = chip_data_parsers_4,
  default = {
    default = function(data) data.op = data.op or VGM_OP_UNKNOWN end,
  },
}

-- support for irregular commands
local chip_special_command_parser = function(data, content, cursor)
  ({
    [0x66] = function()
      data.op = VGM_OP_EODB
    end,

    [0x67] = function()
      data.op = VGM_OP_DATA_BLOCK
      -- TODO check for consistency if it is really 0x66
      local v_0x66 = consumeBytes(1, content, cursor)  -- pick & ignore
      data.data_type, cursor = consumeBytes(1, content, cursor + 1)
      data.data_size, cursor = consumeBytes(4, content, cursor)
      local data_block = {}
      for i=1, data.data_size do
        data_block[1 + #data_block], cursor = consumeBytes(1, content, cursor)
      end
      data.data_block = data_block
    end,

    [0x68] = function()
      data.op = VGM_OP_PCM_RAM_WRITE
      -- TODO check for consistency if it is really 0x66
      local v_0x66 = consumeBytes(1, content, cursor)  -- pick & ignore
      data.data_type, cursor = consumeBytes(1, content, cursor + 1)
      data.data_read_offset, cursor = consumeBytes(3, content, cursor)
      data.data_write_offset, cursor = consumeBytes(3, content, cursor)
      data.data_size, cursor = consumeBytes(3, content, cursor)
    end,
  })[data.cmd]()
  return cursor
end

-- grab following params of command
local chip_data_parser = function(cmd, cmd_info, content, cursor)
  local data = { bytes = {} }
  local bytes = data.bytes
  local grab_bytes = cmd_info[1] or 0

  for i=1, grab_bytes do
    bytes[1 + #bytes], cursor = consumeBytes(1, content, cursor)
  end

  data.cmd = cmd
  data.chip = cmd_info.chip

  if cmd_info.special_command then
    cursor = chip_special_command_parser(data, content, cursor)
  end

  local cdp_pb = chip_data_parsers[grab_bytes] or chip_data_parsers.default
  local cdp = cdp_pb[cmd] or cdp_pb.default

  -- parse the bytes following command to the command data
  cdp(data)  -- i know side effect are generally not good but ...

  data.op = data.op or (data.chip and VGM_OP_REG_WRITE or VGM_OP_OTHER)
  data.port = data.port or cmd_info.port

  return data, cursor
end

-- grab one command with following params
local function consumeCommand(content, cursor)
  local cmd = content:sub(cursor, cursor):byte()
  cursor = 1 + cursor

  local cmd_info = VGM_COMMANDS[cmd] or {}
  local data_parser = cmd_info.data_parser or chip_data_parser
  return data_parser(cmd, cmd_info, content, cursor)
end

-- convert amount of samples to amouont of minutes and seconds
local function _samples_to_time(samples)
  local secs, mins = samples / 44100, 0
  if secs >= 60.0 then
    mins = math.floor(secs / 60)
    secs = secs - mins * 60
  end
  return mins, secs
end

local s_format = string.format  -- make a local for speedup optimization

local formatCommand  -- will be recursive so declare first then assign
formatCommand = function(cmd_data, fmt_data)
  local time_position = cmd_data.time_position or 0
  local mins, secs = _samples_to_time(time_position)
  local result = s_format("%06X %06X %2d:%05.2f ",
    cmd_data.position, time_position, mins, secs)

  if cmd_data.chip then
    result = result .. s_format(" %-20s",
        s_format("%s %2s", cmd_data.chip,
            cmd_data.chip_no and cmd_data.chip_no == 2 and "x2" or ""))
  end

  if cmd_data.op == VGM_OP_REG_WRITE then
    if cmd_data.register == SPECIAL_REGISTER_PSG then
      result = result .. s_format("          <- %04X", cmd_data.value)
    elseif cmd_data.register == SPECIAL_REGISTER_GAMEGEAR_STEREO then
      result = result .. s_format("GG STEREO <- %04X", cmd_data.value)
    elseif cmd_data.value == SPECIAL_DATA_BANK then
      result = result .. s_format("REG #%04X <- DATA_BANK", cmd_data.register)
    else
      result = result .. s_format(
        "REG #%04X <- %04X", cmd_data.register, cmd_data.value
      )
      if cmd_data.port then
        result = result .. s_format("PORT# %d", cmd_data.port)
      end
    end
  elseif cmd_data.op == VGM_OP_MEM_WRITE then
    result = result .. s_format(
        "MEM @%08X <- %04X", cmd_data.address, cmd_data.value
      )
  elseif cmd_data.op == VGM_OP_DATA_BLOCK then
    result = result .. s_format("TODO: DATA BLOCK")
  elseif cmd_data.op == VGM_OP_PCM_RAM_WRITE then
    result = result .. s_format("TODO: PCM WRITE FROM DATA BLOCK")
  elseif cmd_data.op == VGM_OP_EODB then
    result = result .. s_format("END OF DATA BLOCK")
  elseif cmd_data.op == VGM_OP_OTHER and not cmd_data.wait then
    result = result .. s_format("OTHER OPERATION (?)")
  else
    esult = result .. s_format("??UNKNOWN??")
  end

  if cmd_data.wait then
    if cmd_data.chip then
      result = result .. "\n" .. formatCommand {
        cmd = cmd_data.cmd,
        position = cmd_data.position,
        wait = cmd_data.wait,
      }
    else
      result = result .. s_format("%-20s %04X", " [[ WAIT ]]", cmd_data.wait)
    end
  end

  return result
end


local function printCommand(cmd_data)
  print(formatCommand(cmd_data))
end


return {
  consumeCommand = consumeCommand,
  formatCommand = formatCommand,
  printCommand = printCommand,
}

