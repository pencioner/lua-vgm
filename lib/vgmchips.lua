local VGM_CHIP_NAMES = {
  SN764x = "Texas Instruments SN76489/SN76496",
  YM2413 = "Yamaha YM2413 (OPLL)",
  YM2612 = "Yamaha YM2612",
  YM2151 = "Yamaha YM2151",
  YM2203 = "Yamaha YM2203",
  YM2608 = "Yamaha YM2608",
  YM2610 = "Yamaha YM2610",
  YM3812 = "Yamaha YM3812",
  YM3526 = "Yamaha YM3526",
  Y8950 = "Yamaha Y8950",
  YMZ280B = "Yamaha YMZ280B",
  YMF262 = "Yamaha YMF262",
  YMF278B = "Yamaha YMF278B",
  YMF271 = "Yamaha YMF271",
  AY8910 = "Yamaha AY8910",
  K051649 = "Konami K051649 (SCC1)",
  K054539 = "Konami K054539",
  K053260 = "Konami K053260",
  NES_APU = "NES APU",
  uPD7759 = "NEC uPD7759",
  RF5C68 = "Ricoh RF5C68",
  RF5C164 = "Ricoh RF5C164",
  OKIM6258 = "OKIM6258", 
  OKIM6295 = "OKIM6295",
  HuC6280 = "HuC6280",
  SAA1099 = "SAA1099",
  GA20 = "Irem GA20",
  X1_010 = "Seta X1-010",
  ES5503 = "ES5503",
  ES5506 = "ES5505/ES5506",
  C352 = "Namco C352",
  C140 = "C140",
  VSU = "Virtual Boy VSU",
  SCSP = "SCSP",
  QSound = "QSound",
  Pokey = "Pokey",
  WonderSwan = "WonderSwan",
  PWM = "PWM",
  MultiPCM = "MultiPCM",
  Sega_PCM = "Sega PCM",
  GameBoy_DMG = "GameBoy DMG",
}

local VGM_CHIPS = {}
for chip_name, _ in pairs(VGM_CHIP_NAMES) do
  VGM_CHIPS[chip_name] = chip_name
end

return {
  VGM_CHIPS = VGM_CHIPS,
  VGM_CHIP_NAMES = VGM_CHIP_NAMES,
}

