local utils = require "utils"
local consumeInt32 = utils.consumeInt32
local consumeInt16 = utils.consumeInt16
local consumeInt8 = utils.consumeInt8
local table_deepcopy = utils.table_deepcopy

local vgmchips = require 'vgmchips'
local VGM_CHIPS = vgmchips.VGM_CHIPS
local VGM_CHIP_NAMES = vgmchips.VGM_CHIP_NAMES

local VGM_MAGIC = "Vgm "
local VGM_GD3_OFFSET_RELATIVE = 0x14
local VGM_LOOP_OFFSET_RELATIVE = 0x1C
local VGM_DATA_OFFSET_RELATIVE = 0x34
local VGM_DATA_OFFSET_DEFAULT = 0x40
local VGM_EXTRA_HEADER_OFFSET_RELATIVE = 0xBC

local VGM_SN76489_FEEDBACK_DEFAULT = 0x0009
local VGM_SN76489_SHIFT_REGISTER_WIDTH_DEFAULT = 16

local VGM_DEFAULT_VERSION = 171

local __bit31set, __bit31inv = 0x80000000, 0x7FFFFFFF
local __bit30set, __bit30inv = 0x40000000, 0xBFFFFFFF
local __bit3130set, __bit3130inv = 0xC0000000, 0x3FFFFFFF


local VGM_header_normalizers = {
  SN76489_clock = { VGM_CHIPS.SN764x,
    _self_ = function(header)
      local SN76489_clock = header.SN76489_clock
      if SN76489_clock & __bit3130set == __bit3130set then
          header.T6W28_used = true
          SN76489_clock = SN76489_clock & __bit3130inv
      end
      return SN76489_clock
    end,
    SN76489_feedback = function(header)
      return header.SN76489_feedback or VGM_SN76489_FEEDBACK_DEFAULT
    end,
    SN76489_shift_register_width = function(header)
      return header.SN76489_shift_register_width or
                 VGM_SN76489_SHIFT_REGISTER_WIDTH_DEFAULT
    end,
    SN76489_flags = true,
  },
  Sega_PCM_clock = { VGM_CHIPS.Sega_PCM,
    Sega_PCM_interface_register = true,
  },
  YM2203_clock = { VGM_CHIPS.YM2203,
    YM2203_AY8910_flags = true,
  },
  YM2608_clock = { VGM_CHIPS.YM2608,
    YM2608_AY8910_flags = true,
  },
  YM2610_clock = { VGM_CHIPS.YM2610,
    _self_ = function(header)
      if header.YM2610_clock & __bit31set ~= 0 then
        header.YM2610B_used = true
      end
      return header.YM2610_clock & __bit31inv
    end,
  },
  AY8910_clock = { VGM_CHIPS.AY8910,
    AY8910_chip_type = true,
    AY8910_flags = true,
  },
  NES_APU_clock = { VGM_CHIPS.NES_APU,
    _self_ = function (header)
      if header.NES_APU_clock & __bit31set ~= 0 then
        header.NES_APU_FDS_used = true
      end
      return header.NES_APU_clock & __bit31inv
    end,
  },
  OKIM6258_clock = { VGM_CHIPS.OKIM6258,
    OKIM6258_flags = true,
  },
  C140_clock = { VGM_CHIPS.C140,
    C140_chip_type = true,
  },
  K054539_clock = { VGM_CHIPS.K054539,
    K054539_flags = true,
  },
  ES5503_clock = { VGM_CHIPS.ES5503,
    ES5503_channels = true,
  },
  ES5505_ES5506_clock = { VGM_CHIPS.ES5506,
    ES5505_ES5506_channels = true,
    _self_ = function (header)
      if header.ES5505_ES5506_clock & __bit31set ~= 0 then
        header.ES5506_used = true
      end
      return header.ES5505_ES5506_clock & __bit31inv
    end,
  },
  C352_clock = { VGM_CHIPS.C352,
    C352_clock_divider = true,
  },
  YM2413_clock = { VGM_CHIPS.YM2413 },
  YM2612_clock = { VGM_CHIPS.YM2612 },
  YM2151_clock = { VGM_CHIPS.YM2151 },
  RF5C68_clock = { VGM_CHIPS.RF5C68 },
  YM3812_clock = { VGM_CHIPS.YM3812 },
  YM3526_clock = { VGM_CHIPS.YM3526 },
  Y8950_clock = { VGM_CHIPS.Y8950 },
  YMF262_clock = { VGM_CHIPS.YMF262 },
  YMF278B_clock = { VGM_CHIPS.YMF278B },
  YMF271_clock = { VGM_CHIPS.YMF271 },
  YMZ280B_clock = { VGM_CHIPS.YMZ280B },
  RF5C164_clock = { VGM_CHIPS.RF5C164 },
  PWM_clock = { VGM_CHIPS.PWM },
  GameBoy_DMG_clock = { VGM_CHIPS.GameBoy_DMG },
  MultiPCM_clock = { VGM_CHIPS.MultiPCM },
  uPD7759_clock = { VGM_CHIPS.uPD7759 },
  OKIM6295_clock = { VGM_CHIPS.OKIM6295 },
  K051649_clock = { VGM_CHIPS.K051649 },
  HuC6280_clock = { VGM_CHIPS.HuC6280 },
  K053260_clock = { VGM_CHIPS.K053260 },
  Pokey_clock = { VGM_CHIPS.Pokey },
  QSound_clock = { VGM_CHIPS.QSound },
  SCSP_clock = { VGM_CHIPS.SCSP },
  WonderSwan_clock = { VGM_CHIPS.WonderSwan },
  VSU_clock = { VGM_CHIPS.VSU },
  SAA1099_clock = { VGM_CHIPS.SAA1099 },
  X1_010_clock = { VGM_CHIPS.X1_010 },
  GA20_clock = { VGM_CHIPS.GA20 },
}


local function normalizeVgmHeader(header)
  local chips_used = header.chips_used or {}
  header.chips_used = chips_used

  for name, value in pairs(table_deepcopy(header)) do
    local normalizers = VGM_header_normalizers[name]
    if normalizers then
      if value == 0 then
        header[name] = nil
        for relname in pairs(normalizers) do
          if type(relname) == "string" and relname:sub(1,1) ~= "_" then
            header[relname] = nil
          end
        end
      else
        for relname, relfunc in pairs(normalizers) do
          if type(relname) == "string" and relname:sub(1,1) ~= "_"
              and type(relfunc) == "function" then
            header[relname] = relfunc(header)
          end
        end
        if normalizers._self_ then
          header[name] = normalizers._self_(header)
        end

        local chip = normalizers[1]
        if chip then
          chips_used[1 + #chips_used] = chip
          if value & __bit30set ~= 0 then
            header[name] = value & __bit30inv
            chips_used[chip] = 2
          else
            chips_used[chip] = 1
          end
        end
      end
    end
  end
end

local function parseVgmExtraHeader(header, cursor, locate, maxlimiter)
  if header.Extra_header_offset == 0 then
    return header
  end
  header.Extra_header = {}
  locate(header.Extra_header_offset + VGM_EXTRA_HEADER_OFFSET_RELATIVE + 1)
  do
    local eh_size = cursor(consumeInt32)
      -- TODO: Parse extra header
  end

  return header
end

local VGM_chip_clocks = {}
for clock, chip in pairs(VGM_header_normalizers) do
  VGM_chip_clocks[chip[1]] = clock
end

local function header_clock_proxy_metatable(header)
  local proxy = {}

  for chip, count in pairs(header.chips_used) do
    if type(chip) ~= "number" then
      local chip_clock = VGM_chip_clocks[chip]
      local clock = header[chip_clock]
      if chip == VGM_CHIPS.SN764x and header.T6W28_used then
        proxy.SN76489_clock = header.SN76489_clock | __bit3130set
      elseif (chip == VGM_CHIPS.YM2610 and header.YM2610B_used) or
             (chip == VGM_CHIPS.NES_APU and header.NES_APU_FDS_used) or
             (chip == VGM_CHIPS.ES5506 and header.ES5506_used) then
        proxy[chip_clock] = header[chip_clock] | __bit31set
      end
      if count > 1 then  -- dual chip support
        proxy[chip_clock] = header[chip_clock] | __bit30set
      end
    end
  end

  return setmetatable(proxy, {
    __index = function(_, idx)
      return header[idx] or 0
    end,
  })
end

local VGM_header_pack_string = "!1<"
      .."I4 I4 I4 I4 I4 I4 I4 I4"
      .."I4 I4 I2I1I1 I4 I4 I4 I4 I4"
      .."I4 I4 I4 I4 I4 I4 I4 I4"
      .."I4 I4 I4 I4 I4 I4 I1I1I1I1 I1I1I1I1"
      .."I4 I4 I4 I4 I4 I1I1I1I1 I4 I4"
      .."I4 I4 I4 I4 I4 I4 I4 I4"
      .."I4 I4 I4 I4 I4 I2I1I1 I4 I4"
      .."I4 I12 I16"

local VGM_header_value_keys = {
  'VGM_file_magic', 'EOF_offset', 'bcdversion', 'SN76489_clock',
  'YM2413_clock', 'GD3_offset', 'Total_samples', 'Loop_offset',
  'Loop_samples', 'Rate',
      'SN76489_feedback', 'SN76489_shift_register_width', 'SN76489_flags',
      'YM2612_clock',
  'YM2151_clock', 'Data_offset', 'Sega_PCM_clock',
      'Sega_PCM_interface_register',
  'RF5C68_clock', 'YM2203_clock', 'YM2608_clock', 'YM2610_clock',
  'YM3812_clock', 'YM3526_clock', 'Y8950_clock', 'YMF262_clock',
  'YMF278B_clock', 'YMF271_clock', 'YMZ280B_clock', 'RF5C164_clock',
  'PWM_clock', 'AY8910_clock', 'AY8910_chip_type',
      'AY8910_flags', 'YM2203_AY8910_flags', 'YM2608_AY8910_flags',
      'Volume_modifier', 'RESERVED_0x7D', 'Loop_base', 'Loop_modifier',
  'GameBoy_DMG_clock', 'NES_APU_clock', 'MultiPCM_clock', 'uPD7759_clock',
  'OKIM6258_clock', 'OKIM6258_flags', 'K054539_flags', 'C140_chip_type',
      'RESERVED_0x97', 'OKIM6295_clock', 'K051649_clock',
  'K054539_clock', 'HuC6280_clock', 'C140_clock', 'K053260_clock',
  'Pokey_clock', 'QSound_clock', 'SCSP_clock', 'Extra_header_offset',
  'WonderSwan_clock', 'VSU_clock', 'SAA1099_clock', 'ES5503_clock',
  'ES5505_ES5506_clock', 'ES5503_channels', 'ES5505_ES5506_channels',
      'C352_clock_divider', 'RESERVED_0xD7', 
      'X1_010_clock', 'C352_clock',
  'GA20_clock', 'PADDING_12bytes', 'PADDING_16bytes'
}


local VGM_file_magic = 0x206d6756  -- "Vgm " in little endian

local function produceVgmHeader(header, version)
  local pheader = header_clock_proxy_metatable(header)
  pheader.VGM_file_magic = VGM_file_magic
  pheader.bcdversion = tonumber('0x' .. tostring(version or header.version))

  local header_values = {}
  for _, key in pairs(VGM_header_value_keys) do
    header_values[1 + #header_values] = pheader[key]
  end

  local header_content = VGM_header_pack_string:pack(unpack(header_values))

  return header_content
end

local function consumeVgmHeader(cursor, locate, maxlimiter)
  local header = {}

  locate(5)  -- init cursor position to 5 to skip "Vgm "
  header.EOF_offset = cursor(consumeInt32)
  local int32 = cursor(consumeInt32)
  if int32 then
    local maj1, maj2 = (int32 >> 12) & 0xf, (int32 >> 8) & 0xf
    local min1, min2 = (int32 >> 4) & 0xf, int32 & 0xf
    header.major_version = maj1 * 10 + maj2
    header.minor_version = min1 * 10 + min2
  else
    header.major_version, header.minor_version = 0, 0
  end
  local version = header.major_version * 100 + header.minor_version
  header.version = version

  -- V 1.00
  header.SN76489_clock = cursor(consumeInt32)
  header.YM2413_clock = cursor(consumeInt32)
  header.GD3_offset = cursor(consumeInt32)
  header.Total_samples = cursor(consumeInt32)
  header.Loop_offset = cursor(consumeInt32)
  header.Loop_samples = cursor(consumeInt32)
  if version < 101 then
    return header
  end

  -- V 1.01
  header.Rate = cursor(consumeInt32)
  if version == 101 then
    return header
  end

  -- V 1.10
  header.SN76489_feedback = cursor(consumeInt16)
  header.SN76489_shift_register_width = cursor(consumeInt8)
  do
    local SN76489_flags = cursor(consumeInt8)
    if version > 150 then
      -- V 1.51
      header.SN76489_flags = SN76489_flags
    end
  end
  header.YM2612_clock = cursor(consumeInt32)
  header.YM2151_clock = cursor(consumeInt32)
  if version < 150 then
    return header
  end

  -- V 1.50
  header.Data_offset = cursor(consumeInt32)
  maxlimiter(header.Data_offset + VGM_DATA_OFFSET_RELATIVE + 1)
  if version == 150 then
    return header
  end

  -- V 1.51
  header.Sega_PCM_clock = cursor(consumeInt32)
  header.Sega_PCM_interface_register = cursor(consumeInt32)

  header.RF5C68_clock = cursor(consumeInt32)
  header.YM2203_clock = cursor(consumeInt32)
  header.YM2608_clock = cursor(consumeInt32)
  header.YM2610_clock = cursor(consumeInt32)
  header.YM3812_clock = cursor(consumeInt32)
  header.YM3526_clock = cursor(consumeInt32)
  header.Y8950_clock = cursor(consumeInt32)
  header.YMF262_clock = cursor(consumeInt32)
  header.YMF278B_clock = cursor(consumeInt32)
  header.YMF271_clock = cursor(consumeInt32)
  header.YMZ280B_clock = cursor(consumeInt32)
  header.RF5C164_clock = cursor(consumeInt32)
  header.PWM_clock = cursor(consumeInt32)
  header.AY8910_clock = cursor(consumeInt32)
  header.AY8910_chip_type = cursor(consumeInt8)
  header.AY8910_flags = cursor(consumeInt8)
  header.YM2203_AY8910_flags = cursor(consumeInt8)
  header.YM2608_AY8910_flags = cursor(consumeInt8)
  do
    local Volume_modifier = cursor(consumeInt8)
    cursor(consumeInt8)  -- reserved at 0x7D
    local Loop_base = cursor(consumeInt8)
    if version >= 160 then
      -- V 1.60
      header.Volume_modifier = Volume_modifier
      header.Loop_base = Loop_base
    end
  end
  header.Loop_modifier = cursor(consumeInt8)
  if version < 161 then
    return header
  end

  -- V 1.61
  header.GameBoy_DMG_clock = cursor(consumeInt32)
  header.NES_APU_clock = cursor(consumeInt32)
  header.MultiPCM_clock = cursor(consumeInt32)
  header.uPD7759_clock = cursor(consumeInt32)
  header.OKIM6258_clock = cursor(consumeInt32)
  header.OKIM6258_flags = cursor(consumeInt8)
  header.K054539_flags = cursor(consumeInt8)
  header.C140_chip_type = cursor(consumeInt8)
  cursor(consumeInt8)  -- reserved at 0x97
  header.OKIM6295_clock = cursor(consumeInt32)
  header.K051649_clock = cursor(consumeInt32)
  header.K054539_clock = cursor(consumeInt32)
  header.HuC6280_clock = cursor(consumeInt32)
  header.C140_clock = cursor(consumeInt32)
  header.K053260_clock = cursor(consumeInt32)
  header.Pokey_clock = cursor(consumeInt32)
  header.QSound_clock = cursor(consumeInt32)
  if version < 170 then
    return header
  end


  do
    -- V 1.70
    SCSP_clock = cursor(consumeInt32)
    if version > 170 then
      -- V 1.71
      header.SCSP_clock = SCSP_clock
    end
  end
  header.Extra_header_offset = cursor(consumeInt32)
  if version < 171 then
    return parseVgmExtraHeader(header, cursor, locate, maxlimiter)
  end

  -- V 1.71
  header.WonderSwan_clock = cursor(consumeInt32)
  header.VSU_clock = cursor(consumeInt32)
  header.SAA1099_clock = cursor(consumeInt32)
  header.ES5503_clock = cursor(consumeInt32)
  header.ES5505_ES5506_clock = cursor(consumeInt32)
  header.ES5503_channels = cursor(consumeInt8)
  header.ES5505_ES5506_channels = cursor(consumeInt8)
  header.C352_clock_divider = cursor(consumeInt8)
  cursor(consumeInt8)  -- reserved at 0xD7
  header.X1_010_clock = cursor(consumeInt32)
  header.C352_clock = cursor(consumeInt32)
  header.GA20_clock = cursor(consumeInt32)

  return header
end

local s_format = string.format

local _clock_suffix = { 'Hz', 'kHz', 'MHz', 'GHz' }

local humanize_clock
humanize_clock = function(clock, idx)
  idx = idx or 1
  if clock < 1000 then
    return s_format("%.2f%s", clock, _clock_suffix[idx])
  end
  return humanize_clock(clock/1000.0, idx + 1)
end

local function formatVgmHeader(header)
  local result = {}
  local chips_used = header.chips_used
  if #chips_used == 0 then
    return "<? No Chips Used ?>"
  end

  for n, chip in ipairs(chips_used) do
    local line = s_format("Chip #%d: %s (%s)", n, chip, VGM_CHIP_NAMES[chip])
    if chips_used[chip] == 2 then
      line = line .. " [x2]"
    end
    local clock = header[VGM_chip_clocks[chip]]
    line = line .. s_format(" @ %dHz (%s)", clock, humanize_clock(clock))
    result[1 + #result] = line
    if chip == VGM_CHIPS.SN76489 then
      result[1 + #result] = s_format(
        "%9s feedback=%04X shift register width=%d", "",
        header.SN76489_feedback,
        header.SN76489_shift_register_width
      )
      if header.T6W28_used then
        result[1 + #result] = s_format("%9s used T6W28", "")
      end
    elseif chip == VGM_CHIPS.Sega_PCM then
      result[1 + #result] = s_format("%9s interface register = %08X", "",
                                     header.Sega_PCM_interface_register)
    elseif chip == VGM_CHIPS.YM2203 then
      result[1 + #result] = s_format("%9s SSG(AY8910) flags=%02X", "",
                                     header.YM2203_AY8910_flags)
    elseif chip == VGM_CHIPS.YM2608 then
      result[1 + #result] = s_format("%9s SSG(AY8910) flags=%02X", "",
                                     header.YM2608_AY8910_flags)
    elseif chip == VGM_CHIPS.YM2610 and header.YM2610B_used then
      result[1 + #result] = s_format("%9s used YM2610B", "")
    elseif chip == VGM_CHIPS.AY8910 then
      result[1 + #result] = s_format(
        "%9s flags=%02X chip type=%02X", "",
        header.AY8910_flags, header.AY8910_chip_type
      )
    elseif chip == VGM_CHIPS.NES_APU and header.NES_APU_FDS_used then
      result[1 + #result] = s_format("%9s used NES APU FDS", "")
    elseif chip == VGM_CHIPS.OKIM6258 then
      result[1 + #result] = s_format("%9s flags=%02X", "",
                                     header.OKIM6258_flags)
    elseif chip == VGM_CHIPS.C140 then
      result[1 + #result] = s_format("%9s chip type=%02X", "",
                                     header.C140_chip_type)
    elseif chip == VGM_CHIPS.K054539 then
      result[1 + #result] = s_format("%9s flags=%02X", "",
                                     header.K054539_flags)
    elseif chip == VGM_CHIPS.ES5503 then
      result[1 + #result] = s_format("%9s channels=%d", "",
                                     header.ES5503_channels)
    elseif chip == VGM_CHIPS.ES5506 then
      result[1 + #result] = s_format("%9s channels=%d", "",
                                     header.ES5505_ES5506_channels)
      if header.ES5506_used then
        result[1 + #result] = s_format("%9s used ES5506", "")
      end
    elseif chip == VGM_CHIPS.C352 then
      result[1 + #result] = s_format("%9s clock divider=%02X", "",
                                     header.C352_clock_divider)
    end
  end
  result[1 + #result] = ""
  return table.concat(result, "\n")
end

local function printVgmHeader(header)
  print(formatVgmHeader(header))
end
    
return {
  normalizeVgmHeader = normalizeVgmHeader,
  consumeVgmHeader = consumeVgmHeader,
  produceVgmHeader = produceVgmHeader,
  formatVgmHeader = formatVgmHeader,
  printVgmHeader = printVgmHeader,

  VGM_MAGIC = VGM_MAGIC,
  VGM_GD3_OFFSET_RELATIVE = VGM_GD3_OFFSET_RELATIVE,
  VGM_LOOP_OFFSET_RELATIVE = VGM_LOOP_OFFSET_RELATIVE,
  VGM_DATA_OFFSET_RELATIVE = VGM_DATA_OFFSET_RELATIVE,
  VGM_DATA_OFFSET_DEFAULT = VGM_DATA_OFFSET_DEFAULT,
  VGM_EXTRA_HEADER_OFFSET_RELATIVE = VGM_EXTRA_HEADER_OFFSET_RELATIVE,
}


