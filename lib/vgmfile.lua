local zlib = require "zlib"

local chips = require 'vgmchips'
local VGM_CHIPS = chips.VGM_CHIPS

local commands = require 'vgmcommands'
local consumeCommand = commands.consumeCommand

local utils = require "utils"
local consumeInt32 = utils.consumeInt32
local consumeInt16 = utils.consumeInt16
local consumeInt8 = utils.consumeInt8
local contentConsumerCursor = utils.contentConsumerCursor

local vgmheader = require 'vgmheader'
local consumeVgmHeader = vgmheader.consumeVgmHeader
local normalizeVgmHeader = vgmheader.normalizeVgmHeader
local printVgmHeader = vgmheader.printVgmHeader
local VGM_MAGIC = vgmheader.VGM_MAGIC
local VGM_LOOP_OFFSET_RELATIVE = vgmheader.VGM_LOOP_OFFSET_RELATIVE
local VGM_GD3_OFFSET_RELATIVE = vgmheader.VGM_GD3_OFFSET_RELATIVE
local VGM_DATA_OFFSET_RELATIVE = vgmheader.VGM_DATA_OFFSET_RELATIVE
local VGM_DATA_OFFSET_DEFAULT = vgmheader.VGM_DATA_OFFSET_DEFAULT


local function openVgmFile(filename)
  local file = io.open(filename)
  if file then
    local content = file:read("*a")

    if content:sub(1, 4) ~= VGM_MAGIC then
      content = zlib.inflate()(content)
    end

    if content:sub(1, 4) == VGM_MAGIC then
      return content
    end
  end
end


local function parseVgm(file_or_content, is_content)
  local content = file_or_content

  if not is_content then
    local opened_ok
    opened_ok, content = pcall(openVgmFile, file_or_content)
    if not opened_ok or not content then
      return nil, "File open error or wrong file format", content
    end
  end

  local vgm = {}
  local cursor, locate, maxlimiter = contentConsumerCursor(content)

  local header = consumeVgmHeader(cursor, locate, maxlimiter)
  vgm.header = header
  normalizeVgmHeader(header)

  local commands_position = header.version < 150
                       and VGM_DATA_OFFSET_DEFAULT
                       or header.Data_offset + VGM_DATA_OFFSET_RELATIVE
  locate(commands_position + 1)

  if header.GD3_offset then
    maxlimiter(header.GD3_offset + VGM_GD3_OFFSET_RELATIVE + 1)
  else
    maxlimiter(nil)  -- no limits now
  end

  local loop = 0
  if header.Loop_offset > 0 then
    loop = vgm.header.Loop_offset + VGM_LOOP_OFFSET_RELATIVE
  end

  local data = {}
  local time_position = 0
  while true do
    local _data = cursor(consumeCommand)

    if not _data then
      break
    end
    local position = locate() - 1
    _data.position = position
    if position == loop then
      _data.is_loop_point = position
    end
    _data.time_position = time_position
    data[1 + #data] = _data
    time_position = time_position + (_data.wait or 0)
  end
  vgm.data = data

  return vgm, content
end


local function printVgm(vgm)
  if not (vgm or {}).data then
    print "<EMPTY>"
    return
  end

  printVgmHeader(vgm.header)

  for _, v in pairs(vgm.data) do
    if v.is_loop_point then
      print "----------------------- LOOP"
    end
    commands.printCommand(v)
  end
end

return {
  parseVgm = parseVgm,
  printVgm = printVgm,
}

