local s_pack = string.pack

local vgmheader = require 'vgmheader'
local produceVgmHeader = vgmheader.produceVgmHeader
local VGM_DATA_OFFSET_RELATIVE = vgmheader.VGM_DATA_OFFSET_RELATIVE
local VGM_LOOP_OFFSET_RELATIVE = vgmheader.VGM_LOOP_OFFSET_RELATIVE

local raw_produce_plain_cmd = function(data)
  return s_pack("I1", data.cmd)
end

local raw_produce_wait_cmd = function(data)
  return s_pack("!1<I1I2", data.cmd, data.wait)
end

local raw_produce_dd_cmd = function(data)
  return s_pack("I1I1", data.cmd, data.value)
end

local raw_produce_rr_dd_cmd = function(data)
  return s_pack("I1I1I1", data.cmd, data.register, data.value)
end

local raw_produce_DUAL_rr_dd_cmd = function(data)
  local register = data.register
  if data.chip_no == 2 then
    register = register | 0x80
  end
  return s_pack("I1I1I1", data.cmd, register, data.value)
end


local VGM_COMMAND_PRODUCERS = {
  [0x30] = raw_produce_dd_cmd,
  [0x3F] = raw_produce_dd_cmd,
  [0x4F] = raw_produce_dd_cmd,
  [0x50] = raw_produce_dd_cmd,
  [0x51] = raw_produce_rr_dd_cmd,
  [0x52] = raw_produce_rr_dd_cmd,
  [0x53] = raw_produce_rr_dd_cmd,
  [0x54] = raw_produce_rr_dd_cmd,
  [0x55] = raw_produce_rr_dd_cmd,
  [0x56] = raw_produce_rr_dd_cmd,
  [0x57] = raw_produce_rr_dd_cmd,
  [0x58] = raw_produce_rr_dd_cmd,
  [0x59] = raw_produce_rr_dd_cmd,
  [0x5A] = raw_produce_rr_dd_cmd,
  [0x5B] = raw_produce_rr_dd_cmd,
  [0x5C] = raw_produce_rr_dd_cmd,
  [0x5D] = raw_produce_rr_dd_cmd,
  [0x5E] = raw_produce_rr_dd_cmd,
  [0x5F] = raw_produce_rr_dd_cmd,
  [0x61] = raw_produce_wait_cmd,
  [0x62] = raw_produce_plain_cmd,
  [0x63] = raw_produce_plain_cmd,
  [0x64] = { 3, not_implemented = true },
  [0x66] = raw_produce_plain_cmd,
  [0x67] = { special_command = true },
  [0x68] = { special_command = true },

  [0x70] = raw_produce_plain_cmd,
  [0x71] = raw_produce_plain_cmd,
  [0x72] = raw_produce_plain_cmd,
  [0x73] = raw_produce_plain_cmd,
  [0x74] = raw_produce_plain_cmd,
  [0x75] = raw_produce_plain_cmd,
  [0x76] = raw_produce_plain_cmd,
  [0x77] = raw_produce_plain_cmd,
  [0x78] = raw_produce_plain_cmd,
  [0x79] = raw_produce_plain_cmd,
  [0x7A] = raw_produce_plain_cmd,
  [0x7B] = raw_produce_plain_cmd,
  [0x7C] = raw_produce_plain_cmd,
  [0x7D] = raw_produce_plain_cmd,
  [0x7E] = raw_produce_plain_cmd,
  [0x7F] = raw_produce_plain_cmd,
  [0x80] = raw_produce_plain_cmd,
  [0x81] = raw_produce_plain_cmd,
  [0x82] = raw_produce_plain_cmd,
  [0x83] = raw_produce_plain_cmd,
  [0x84] = raw_produce_plain_cmd,
  [0x85] = raw_produce_plain_cmd,
  [0x86] = raw_produce_plain_cmd,
  [0x87] = raw_produce_plain_cmd,
  [0x88] = raw_produce_plain_cmd,
  [0x89] = raw_produce_plain_cmd,
  [0x8A] = raw_produce_plain_cmd,
  [0x8B] = raw_produce_plain_cmd,
  [0x8C] = raw_produce_plain_cmd,
  [0x8D] = raw_produce_plain_cmd,
  [0x8E] = raw_produce_plain_cmd,
  [0x8F] = raw_produce_plain_cmd,
  [0xA0] = raw_produce_DUAL_rr_dd_cmd,

  [0xA1] = raw_produce_rr_dd_cmd,
  [0xA2] = raw_produce_rr_dd_cmd,
  [0xA3] = raw_produce_rr_dd_cmd,
  [0xA4] = raw_produce_rr_dd_cmd,
  [0xA5] = raw_produce_rr_dd_cmd,
  [0xA6] = raw_produce_rr_dd_cmd,
  [0xA7] = raw_produce_rr_dd_cmd,
  [0xA8] = raw_produce_rr_dd_cmd,
  [0xA9] = raw_produce_rr_dd_cmd,
  [0xAA] = raw_produce_rr_dd_cmd,
  [0xAB] = raw_produce_rr_dd_cmd,
  [0xAC] = raw_produce_rr_dd_cmd,
  [0xAD] = raw_produce_rr_dd_cmd,
  [0xAE] = raw_produce_rr_dd_cmd,
  [0xAF] = raw_produce_rr_dd_cmd,


-- TODO FIXME not implemented
  [0xB0] = function() return "" end,
  [0xB1] = function() return "" end,
  [0xB2] = function() return "" end,
  [0xB3] = function() return "" end,
  [0xB4] = function() return "" end,
  [0xB5] = function() return "" end,
  [0xB6] = function() return "" end,
  [0xB7] = function() return "" end,
  [0xB8] = function() return "" end,
  [0xB9] = function() return "" end,
  [0xBA] = function() return "" end,
  [0xBB] = function() return "" end,
  [0xBC] = function() return "" end,
  [0xBD] = function() return "" end,
  [0xBE] = function() return "" end,
  [0xBF] = function() return "" end,
  [0xC0] = function() return "" end,
  [0xC1] = function() return "" end,
  [0xC2] = function() return "" end,
  [0xC3] = function() return "" end,
  [0xC4] = function() return "" end,
  [0xC5] = function() return "" end,
  [0xC6] = function() return "" end,
  [0xC7] = function() return "" end,
  [0xC8] = function() return "" end,
  [0xD0] = function() return "" end,
  [0xD1] = function() return "" end,
  [0xD2] = function() return "" end,
  [0xD3] = function() return "" end,
  [0xD4] = function() return "" end,
  [0xD5] = function() return "" end,
  [0xD6] = function() return "" end,
  [0xE0] = function() return "" end,
  [0xE1] = function() return "" end,
}


local function produceVgm(vgm)
  local content = {}
  local loop_content_offset = 0

  for _, cmd_data in ipairs(vgm.data) do
    if cmd_data.is_loop_point then
      -- fix loop offset, TODO FIXME optimize?
      loop_content_offset = #table.concat(content, "")
    end

    content[1 + #content] = VGM_COMMAND_PRODUCERS[cmd_data.cmd](cmd_data)
  end


  local content_bin = table.concat(content, "")

  -- fix offsets and regenerate header again
  local header = vgm.header
  local header_bin = produceVgmHeader(header, 171)
  local lenk = #header_bin
  header.Data_offset = lenk - VGM_DATA_OFFSET_RELATIVE
  header.EOF_offset = -4 + lenk + #content_bin
  if loop_content_offset > 0 then
    header.Loop_offset = loop_content_offset + lenk - VGM_LOOP_OFFSET_RELATIVE
  end
  header_bin = produceVgmHeader(header, 171)

  return header_bin .. content_bin
end

return {
  produceVgm = produceVgm,
}

