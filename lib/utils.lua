local table_deepcopy
table_deepcopy = function(src, dst)
  local dst = dst or {}
  for k, v in pairs(src) do
    if type(k) == "table" then
      k = table_deepcopy(k)
    end
    if type(v) == "table" then
      v = table_deepcopy(v)
    end
    dst[k] = v
  end
  return dst
end


-- partial call with one argument
local function make_partial_1(f, arg1)
  return function(...)
    return f(arg1, ...)
  end
end


local function consumeBytes(bytes, content, cursor)
  local value = 0
  local peek_only = bytes < 0

  if peek_only then
    bytes = -bytes
  end

  for idx = bytes - 1, 0, -1 do
    local pos = cursor + idx
    local char = content:sub(pos, pos)
    if char == "" then
      return
    end
    value = value * 256 + char:byte()
  end
  return value, cursor + (peek_only and 0 or bytes)
end

local consumeInt32 = make_partial_1(consumeBytes, 4)
local consumeInt16 = make_partial_1(consumeBytes, 2)
local consumeInt8 = make_partial_1(consumeBytes, 1)
local peekInt8 = make_partial_1(consumeBytes, -1)

local function contentConsumerCursor(content, cursor)
  cursor = cursor or 1
  local max_cursor
  local function _contentConsumerCursor(consume_func)
    local value
    if not cursor or cursor > #content or 
        (max_cursor and cursor >= max_cursor) then
      return nil, cursor
    end
    value, cursor = consume_func(content, cursor)
    return value, cursor
  end

  local function _contentLocate(newpos)
    if cursor then
      local oldpos = cursor
      cursor = newpos or oldpos
      return oldpos
    end
  end

  local function _setMaxCursor(new_max_cursor)
    max_cursor = new_max_cursor
  end

  return _contentConsumerCursor, _contentLocate, _setMaxCursor
end


return {
  table_deepcopy = table_deepcopy,
  make_partial_1 = make_partial_1,
  consumeBytes = consumeBytes,
  consumeInt32 = consumeInt32,
  consumeInt16 = consumeInt16,
  consumeInt8 = consumeInt8,
  contentConsumerCursor = contentConsumerCursor,
}

